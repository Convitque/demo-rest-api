const express = require('express');

const router = express.Router();

const mongoose = require("mongoose");

const Order = require("../models/order");
const Product = require("../models/product");

//* Get all
router.get('/', (req, res, next) => {
  Order.find()
    .select("quantity _id product")
    .populate('product')
    .exec()
    .then(docs => {
      res.status(200).json({
        count: docs.length,
        orders: docs.map(doc => {
          return {
            _id: doc._id,
            product: doc.product,
            quantity: doc.quantity,
            request: {
              type: "GET",
              url: "http://localhost:300/orders/" + doc._id
            }
          }
        })

      });
    }).catch(err => {
      res.status(500).json({
        error: err
      });
    })
})


//TODO  Post method
router.post('/', (req, res, next) => {
  Product.findById(req.body.productID)
    .then(product => {
      if (!product) {
        return res.status(404).json({
          message: "Product not found"
        });
      }

      const order = new Order({
        _id: new mongoose.Types.ObjectId,
        quantity: req.body.quantity,
        product: req.body.productID
      });
      order
        .save()
        .then(result => {
          console.log(result);
          res.status(201).json({
            message: "Order stored",
            createdOrder: {
              _id: result._id,
              product: result.product,
              quantity: result.quantity
            },
            request: {
              type: "GET",
              url: "http://localhost:3000/orders" + result._id
            }
          });
        })
    })
    .catch(err => {
      res.status(500).json({
        message: " Product not found",
        error: err
      });
    })
})

router.get('/:orderID', (req, res, next) => {
  const id = req.params.orderID;
  Order.findById(id)
  .select("quantity _id product")
  .exec()
  .then(doc => {
    console.log("from database", doc);
    res.status(200).json({
      order: doc,
      request: {
        type: "GET",
        url: "http://localhost:3000/orders"+ doc._id
      }
    });
  })
  .catch(err => {
    res.status(500).json({
      error: err
    });
  })
})

router.delete('/:orderID', (req, res, next) => {
  const id = req.params.orderID;
  Order.remove({_id: id})
    .exec()
    .then(result => {
      if(!result) {
        return res.status(404).json({
          message: "Order not found"
        });
      }

      res.status(200).json({
        message: "Order deleted",
        request: {
          type: "DELETE"
        }
      });
    })
})

module.exports = router;